import os
import json
import Settings
# import configparser
from PySide6.QtCore import QFile, QIODevice, QDateTime, QPointF
from PySide6.QtUiTools import QUiLoader
from PySide6.QtGui import QImage, QPainter, QPolygonF
from PySide6.QtWidgets import QFileDialog, QDateTimeEdit
from MsgHelper import popup_yes_no, popup_ok, AnswerType
from cv2 import imread
from xml.dom import minidom


loaded_file = ""


def load_ui_file(filename):
    """Loads the given .ui file from the ./ui folder"""
    dir = os.path.join(os.getcwd(), 'data')
    ui_file_name = os.path.join(dir, "ui", filename)

    # Load the .ui file
    ui_file = QFile(ui_file_name)
    if not ui_file.open(QIODevice.ReadOnly):
        print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
        return

    loader = QUiLoader()
    ui = loader.load(ui_file)
    ui_file.close()

    return ui


def save_to_json(editor, image, annotations):
    # If this file hasn't been saved yet, promt with folder popup
    if loaded_file == "" or loaded_file == "Unsaved":
        save_to_json_as(editor, image, annotations)
        return

    save_data = get_json_format(image, annotations)
    save_file(save_data)


def save_to_json_as(editor, image, annotations):
    file, _ = QFileDialog.getSaveFileName(
        editor, 'Save File', Settings.save_folder, "Visionator Files(*.cvis)")
    if file:
        global loaded_file
        loaded_file = file

        save_data = get_json_format(image, annotations)
        save_file(save_data)
        editor.window.setWindowTitle(f"Visionator - {os.path.split(file)[1]}")


def save_file(data_json):
    file = open(loaded_file, 'w')
    json.dump(data_json, file, indent=4)
    popup_ok("Information", "File saved", f'''
File saved to:
{loaded_file}
    ''')


def load_json(editor):
    """
    load custom json file functionality
    """
    img = None

    file, _ = QFileDialog.getOpenFileName(
        editor, "Open a file", Settings.save_folder,
        "Visionator Files(*.cvis)")

    if file == "":
        return

    global loaded_file
    loaded_file = file
    head, tail = os.path.split(file)
    editor.window.setWindowTitle(f"Visionator - {tail}")

    with open(file) as data_file:
        data = json.load(data_file)
        img = data["images"][0]["file_name"]
    editor.reset_annotations()
    editor.draw_all_annotations(data)
    return img


def loadJsonWarning():
    retval = popup_yes_no("Information", "File found",
                          """
This image has previously been saved with annotations.
Would you like to load these annotations?
                          """)

    if retval == AnswerType.Yes:
        return True
    else:
        return False


def export_image_with_data(editor):
    area = editor.scene().sceneRect()
    image = QImage(area.width(), area.height(),
                   QImage.Format_ARGB32_Premultiplied)
    painter = QPainter(image)
    editor.scene().render(painter, image.rect(), area)

    painter.end()

    file, _ = QFileDialog.getSaveFileName(
        editor, 'Save File', Settings.export_folder, "JPEG image (*.JPEG)")
    if file:
        image.save(file, "jpeg")


def export_as_coco(editor, image, annotations):
    data_coco = get_coco_format(editor, image, annotations)

    current_directory = os.getcwd()
    jsondir = os.path.join(current_directory, Settings.export_folder)
    if not os.path.exists(jsondir):
        os.makedirs(jsondir)

    file, _ = QFileDialog.getSaveFileName(
        editor, 'Save File', Settings.export_folder, "JSON Files(*.json)")
    if file:
        f = open(file, 'w')
        json.dump(data_coco, f, indent=4)


def export_as_xml(editor, image):
    data_xml: str = get_xml_format(editor.annotation_data, image)
    full_img_path = os.path.join(Settings.IMAGES_DIR, image)

    file, _ = QFileDialog.getSaveFileName(
        editor, 'Export XML',
        os.path.splitext(full_img_path)[0], "XML Files(*.xml)")
    with open(file, 'w') as f:
        f.write(data_xml)


def get_xml_format(annotation_data, image) -> str:
    full_img_path = os.path.join(Settings.IMAGES_DIR, image)

    doc = minidom.Document()
    root = doc.createElement("annotation")
    doc.appendChild(root)

    folder = create_xml_element(doc, "folder", "images")
    root.appendChild(folder)

    file_name = create_xml_element(doc, "filename", image)
    root.appendChild(file_name)

    path = create_xml_element(
        doc, "path", full_img_path)
    root.appendChild(path)

    source = create_xml_element(doc, "source", "")
    database = create_xml_element(doc, "database", "Unknown")
    source.appendChild(database)
    root.appendChild(source)

    size = create_xml_element(doc, "size", "")
    img = imread(full_img_path)
    h, w, d = img.shape
    width = create_xml_element(doc, "width", str(w))
    height = create_xml_element(doc, "height", str(h))
    depth = create_xml_element(doc, "depth", str(d))
    size.appendChild(width)
    size.appendChild(height)
    size.appendChild(depth)
    root.appendChild(size)

    segmented = create_xml_element(doc, "segmented", "0")
    root.appendChild(segmented)

    for annotation in annotation_data:
        is_polygon = False if annotation.get("polygon") is None else True

        object = create_xml_element(doc, "object", "")
        name = create_xml_element(doc, "name", annotation["name"])
        pose = create_xml_element(doc, "pose", "Unspecified")
        truncated = create_xml_element(doc, "truncated", "0")
        difficult = create_xml_element(doc, "difficult", "0")
        bndbox = create_xml_element(doc, "bndbox", "")

        object.appendChild(name)
        object.appendChild(pose)
        object.appendChild(truncated)
        object.appendChild(difficult)

        if is_polygon:
            pass

        xmin = create_xml_element(doc, "xmin", str(int(annotation["x"])))
        ymin = create_xml_element(doc, "ymin", str(int(annotation["y"])))
        xmax = create_xml_element(doc, "xmax", str(
            int(annotation["x"] + annotation["width"])))
        ymax = create_xml_element(doc, "ymax", str(
            int(annotation["y"] + annotation["height"])))

        bndbox.appendChild(xmin)
        bndbox.appendChild(ymin)
        bndbox.appendChild(xmax)
        bndbox.appendChild(ymax)
        object.appendChild(bndbox)

        root.appendChild(object)

    xml_string = root.toprettyxml(indent="\t")
    return xml_string


def create_xml_element(doc: minidom.Document, element_name: str, text: str):
    '''
    doc: XML document to create an element with
    element_name: Name of the element to create
    text: Internal text of the element. Pass in empty string to not create text

    Creates an XML element and returns it
    '''
    element = doc.createElement(element_name)
    if text != "":
        element.appendChild(doc.createTextNode(text))
    return element


def get_json_format(image, annotations):
    data_json = {}

    img = {}
    img["file_name"] = image

    imgArr = []
    imgArr.append(img)
    data_json["images"] = imgArr
    data_json["annotations"] = annotations
    return data_json


def get_coco_format(editor, image, annotations):
    # print(annotations)
    data_coco = {}
    data_coco["info"] = {}
    data_coco["info"]["contributor"] = "Visionator"
    data_coco["info"]["date_created"] = QDateTimeEdit().textFromDateTime(
        QDateTime.currentDateTime())

    img = {}
    img["file_name"] = image
    img["id"] = 0  # Always 0 since we only support one image at a time

    imgArr = []
    imgArr.append(img)
    data_coco["images"] = imgArr

    i = 0
    j = 0
    categories = []
    coco_annotations = []
    for ann in annotations:
        global double
        double = False
        for category in categories:
            if category["name"] == ann["name"]:
                double = True
        if not double:
            new_category = {}
            new_category["supercategory"] = ann["category"]
            new_category["id"] = j
            new_category["name"] = ann["name"]
            categories.append(new_category)
            j += 1

        annotation = {}
        annotation["id"] = i
        for cat in categories:
            if cat["name"] == ann["name"]:
                annotation["category_id"] = cat["id"]
        annotation["iscrowd"] = 0  # We don't support crowds
        annotation["segmentation"] = []
        annotation["image_id"] = 0  # Again, always 0
        annotation["area"] = 0  # Belongs to the area of segmentation
        annotation["bbox"] = [ann["x"], ann["y"], ann["width"], ann["height"]]

        if "polygon" in ann:
            segment = []
            polygon = QPolygonF()
            for point in ann["polygon"]:
                # For the segmentation
                segment.append(point[0])
                segment.append(point[1])

                # For calculating the area
                p = QPointF(point[0], point[1])
                polygon.append(p)

            annotation["segmentation"].append(segment)
            annotation["area"] = calculate_area(polygon)

        coco_annotations.append(annotation)
        i += 1

    data_coco["annotations"] = coco_annotations
    data_coco["categories"] = categories

    return data_coco


def calculate_area(qpolygon):
    """Calculates the area of the given polygon"""
    area = 0
    for i in range(qpolygon.size()):
        p1 = qpolygon[i]
        p2 = qpolygon[(i + 1) % qpolygon.size()]
        d = p1.x() * p2.y() - p2.x() * p1.y()
        area += d
    return area
