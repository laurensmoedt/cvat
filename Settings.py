import Loading
import configparser
import os
import platform
from PySide6.QtWidgets import QApplication, QFileDialog
from PySide6.QtGui import QIcon

import qdarkstyle  # pip install qdarkstyle
import qdarktheme  # pip install pyqtdarktheme

IMAGES_DIR: str = os.path.join(os.getcwd(), 'data', 'images')

window = None
theme = 0
theme_tmp = 0
save_folder = ""
import_folder = ""
export_folder = ""

settings_file = os.path.join("data", "settings.cfg")


def init():
    '''
    Initializes the settings
    '''
    global window

    # Load the settings UI and set the window title/icon
    window = Loading.load_ui_file("settings.ui")
    window.setWindowTitle("Settings")
    window.setWindowIcon(QIcon("icon.png"))

    # Prepare the buttons
    connect_signals()

    # Load settings from file
    load_settings()

    # Create the default directories if they dont exist
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    if not os.path.exists(import_folder):
        os.makedirs(import_folder)

    if not os.path.exists(export_folder):
        os.makedirs(export_folder)


def connect_signals():
    '''
    Connects the buttons in the settings window to a method
    '''
    window.themeEdit.currentIndexChanged.connect(_on_theme_changed)
    window.applyTheme.clicked.connect(_on_apply_theme)

    window.saveEdit.textChanged.connect(_on_save_folder_changed)
    window.saveBrowse.clicked.connect(_on_save_browse)

    window.importEdit.textChanged.connect(_on_import_folder_changed)
    window.importBrowse.clicked.connect(_on_import_browse)

    window.exportEdit.textChanged.connect(_on_export_folder_changed)
    window.exportBrowse.clicked.connect(_on_export_browse)


def load_settings():
    '''
    Loads the settings stored in the settings.cfg file
    '''
    config = configparser.ConfigParser()
    config.read(settings_file)

    global theme
    global save_folder
    global import_folder
    global export_folder

    if config.has_section("Settings"):
        for option in config.options("Settings"):
            # Get all data from the settings file and store it
            match option:
                case "theme":
                    theme = config.getint("Settings", "theme")

                case "default_save_folder":
                    save_folder = config.get("Settings", "default_save_folder")

                case "default_import_folder":
                    import_folder = config.get(
                        "Settings", "default_import_folder")

                case "default_export_folder":
                    export_folder = config.get(
                        "Settings", "default_export_folder")

    # If the variables are still empty, set it to default
    platform_name = platform.system().lower()
    print(platform_name)

    baseFolder = os.getcwd()
    if save_folder == "":
        save_folder = os.path.join(baseFolder, "saves")
    if import_folder == "":
        if platform_name == "linux" or platform_name == "linux2" or platform_name == "darwin":
            # If run on linux or mac:
            import_folder = os.path.expanduser(os.sep.join(["~", "Pictures"]))
        elif platform_name == "win32" or platform_name == "cygwin" or platform_name == "windows":
            # If run on windows, or under cygwin on windows:
            import_folder = os.path.join(os.environ["USERPROFILE"], "Pictures")
        else:
            raise OSError("Unknown/Unsupported platform")
    if export_folder == "":
        export_folder = baseFolder

    # Set the items on screen to the corresponding data
    window.themeEdit.setCurrentIndex(theme)
    window.saveEdit.setText(save_folder)
    window.importEdit.setText(import_folder)
    window.exportEdit.setText(export_folder)

    set_theme(theme)


def save_settings():
    '''
    Saves the settings to file
    '''
    config = configparser.ConfigParser()

    # Create a dictionary containing the data
    config["Settings"] = {
        "theme": theme,
        "default_save_folder": save_folder,
        "default_import_folder": import_folder,
        "default_export_folder": export_folder
    }

    # Write it to file
    with open(settings_file, 'w') as file:
        config.write(file)


def set_theme(index):
    '''
    index: index of the theme that is to be loaded

    Sets the theme to the theme at the given index
    '''
    app = QApplication.instance()
    match theme_tmp:
        case 0:  # Light
            app.setStyleSheet("")
        case 1:  # Dark
            app.setStyleSheet(qdarktheme.load_stylesheet())
        case 2:  # Dark blue
            app.setStyleSheet(qdarkstyle.load_stylesheet())


def show():
    window.show()


def _on_theme_changed(index):
    '''
    Called when the user changes the combobox of the theme
    '''
    global theme_tmp
    theme_tmp = index


def _on_apply_theme():
    '''
    Called when the user applies the theme
    '''
    global theme
    theme = theme_tmp
    set_theme(theme)
    save_settings()


def _on_save_folder_changed(folder):
    '''
    Called when user changes the default save folder
    '''
    global save_folder
    save_folder = folder
    save_settings()


def _on_save_browse():
    '''
    Called when the user hits 'Browse' on the save folder
    '''
    global save_folder

    dir = QFileDialog.getExistingDirectory(
        None, 'Select save folder', save_folder, QFileDialog.ShowDirsOnly)
    if dir == "":
        return

    save_folder = dir
    window.saveEdit.setText(dir)
    save_settings()


def _on_import_folder_changed(folder):
    '''
    Called when user changes the import save folder
    '''
    global import_folder
    import_folder = folder
    save_settings()


def _on_import_browse():
    '''
    Called when the user hits 'Browse' on the import folder
    '''
    global import_folder

    dir = QFileDialog.getExistingDirectory(
        None, 'Select import folder', import_folder, QFileDialog.ShowDirsOnly)
    if dir == "":
        return

    import_folder = dir
    window.importEdit.setText(dir)
    save_settings()


def _on_export_folder_changed(folder):
    '''
    Called when user changes the default export folder
    '''
    global export_folder
    export_folder = folder
    save_settings()


def _on_export_browse():
    '''
    Called when the user hits 'Browse' on the export folder
    '''
    global export_folder

    dir = QFileDialog.getExistingDirectory(
        None, 'Select export folder', export_folder, QFileDialog.ShowDirsOnly)
    if dir == "":
        return

    export_folder = dir
    window.exportEdit.setText(dir)
    save_settings()
