import os
from PySide6.QtWidgets import QHBoxLayout, QPushButton, QLineEdit


dir = os.path.join(os.getcwd(), 'data')
catFile = os.path.join(dir, 'categories')


class CategoryEdit(QHBoxLayout):
    '''
    name: string name of the category

    Class that creates a line edit and delete button for a category
    '''
    delButton = None
    lineEdit = None
    categoryName = ""

    def __init__(self, name):
        super().__init__()
        self.categoryName = name

        # Create the line edit
        self.lineEdit = QLineEdit()
        self.lineEdit.setText(name)
        self.lineEdit.setReadOnly(True)
        self.lineEdit.textChanged.connect(self._on_name_changed)
        self.addWidget(self.lineEdit)

        # Create the delete button
        self.delButton = QPushButton()
        self.delButton.setText("Delete")
        self.delButton.setMaximumWidth(60)
        self.delButton.setVisible(False)
        self.delButton.pressed.connect(self.delete)
        self.addWidget(self.delButton)

    def toggle_edit_mode(self):
        '''
        Toggles the visibility of the delete button and
        whether the category name can be edited
        '''
        if self.delButton.isVisible():
            self.delButton.setVisible(False)
        else:
            self.delButton.setVisible(True)

        if self.lineEdit.isReadOnly():
            self.lineEdit.setReadOnly(False)
        else:
            self.lineEdit.setReadOnly(True)

    def get_category(self):
        '''
        Returns the name of this category
        '''
        return self.categoryName

    def delete(self):
        '''
        Deletes this category object
        '''
        self.delButton.deleteLater()
        self.lineEdit.deleteLater()
        self.deleteLater()

    def _on_name_changed(self, name):
        '''
        name: the new name of this category

        Gets called when the name of the category
        is changed by the user
        '''
        self.categoryName = name


def store_categories(categories):
    '''
    categoires: list of categories to store

    Stores the list into a .txt file without an extension
    '''
    with open(catFile, 'w') as c:
        for cat in categories:
            c.write(f"{cat}\n")


def load_categories(window):
    '''
    window: QMainWindow to reference objects in the window

    Loads and sets the categories stored in file

    Returns a list of category names
    '''
    if not os.path.exists(catFile):
        with open(catFile, 'w') as f:
            f.write("")
            f.close()

    categories = []
    with open(catFile, 'r') as c:
        for line in c:
            cat = line[:-1]
            categories.append(cat)

    for name in categories:
        layout = CategoryEdit(name)
        window.Categories.insertLayout(-1, layout, 0)
        window.catEdit.addItem(name)

    return categories
