import os
# import configparser
import shutil
import webbrowser
import Loading
import Settings
from Editor import Mode
from Category import CategoryEdit, store_categories
from MsgHelper import popup_yes_no, AnswerType
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QColorDialog, QFileDialog

# Some variables for easy references
window = None
editor = None
image = None
imagePath = None


def connect_signals():
    '''
    Connects most buttons in the UI to a method
    '''
    window.actionShowHelp.triggered.connect(_on_help_clicked)

    window.import_image.triggered.connect(_on_import_clicked)
    window.exportImage.triggered.connect(_on_export_image_clicked)
    window.exportCoco.triggered.connect(_on_export_coco_clicked)
    window.exportXML.triggered.connect(_on_export_xml_clicked)
    window.actionSave.triggered.connect(_on_save_clicked)
    window.menuOpen.triggered.connect(_on_load_clicked)
    window.actionSaveAs.triggered.connect(_on_save_as_clicked)
    window.actionSettings.triggered.connect(_on_settings_clicked)

    window.actionUndo.triggered.connect(_on_undo_clicked)
    window.actionRedo.triggered.connect(_on_redo_clicked)

    window.actionSelect.triggered.connect(_on_tool_select)
    window.actionCreate.triggered.connect(_on_tool_create)
    window.actionEdit.triggered.connect(_on_tool_edit)
    window.actionPan.triggered.connect(_on_tool_pan)
    window.actionCreatePolygon.triggered.connect(_on_tool_create_polygon)

    window.DeleteAnnotation.pressed.connect(_on_delete_annotation)
    window.nameEdit.textChanged.connect(editor._on_name_changed)
    window.nameEdit.returnPressed.connect(_on_name_changed)
    window.catEdit.currentTextChanged.connect(editor._on_category_changed)
    window.xEdit.valueChanged.connect(editor._on_x_changed)
    window.yEdit.valueChanged.connect(editor._on_y_changed)
    window.widthEdit.valueChanged.connect(editor._on_width_changed)
    window.heightEdit.valueChanged.connect(editor._on_height_changed)
    window.colorEdit.pressed.connect(_on_color_pressed)

    window.viewProperties.toggled.connect(_on_view_properties_toggled)
    window.viewOutliner.toggled.connect(_on_view_outliner_toggled)
    window.viewCategories.toggled.connect(_on_view_categories_toggled)

    window.createCatButton.pressed.connect(_on_create_category)
    window.editCategories.toggled.connect(_on_edit_category)
    window.catName.textChanged.connect(_on_cat_name_changed)

    window.deleteSelectedAnnotations.pressed.connect(_on_delete_all)

    window.polyBoxShow.stateChanged.connect(editor._on_poly_box_show_toggled)
    window.polyBoxShow.hide()


def _on_view_properties_toggled(toggled):
    '''
    toggled: bool whether the button is enabled or not

    Sets the visibility of the properties dock
    '''
    window.PropertiesDock.setVisible(toggled)


def _on_view_outliner_toggled(toggled):
    '''
    toggled: bool whether the button is enabled or not

    Sets the visibility of the outliner dock
    '''
    window.OutlinerDock.setVisible(toggled)


def _on_view_categories_toggled(toggled):
    '''
    toggled: bool whether the button is enabled or not

    Sets the visibility of the categories dock
    '''
    window.CategoryDock.setVisible(toggled)


def _on_cat_name_changed(new_text):
    if new_text == "":
        window.createCatButton.setEnabled(False)
    else:
        window.createCatButton.setEnabled(True)


def _on_create_category():
    '''
    Creates a new category and saves it to disk
    '''
    catName = window.catName.text()
    window.catName.clear()

    edit = CategoryEdit(catName)

    window.Categories.insertLayout(-1, edit, 0)

    save_categories()


def save_categories():
    '''
    Sets the new categories to the category property editor and
    saves them to disk
    '''
    # Remove previous categories from the property window
    window.catEdit.clear()
    categories = []

    # Loop through the categories and add them back to the property window
    count = window.Categories.count()
    for i in range(count):
        name = window.Categories.itemAt(i).get_category()
        window.catEdit.addItem(name)
        categories.append(name)

    # Store them to disk - method from Categories.py
    store_categories(categories)


def _on_edit_category(toggled):
    '''
    Called when the user clicks edit categories
    Will enable renaming and deleting of categories

    When hit again as a save button saves the categories
    '''
    if toggled:
        window.createCatButton.setEnabled(False)
        window.catName.setEnabled(False)
        window.editCategories.setText("Save")
    else:
        window.createCatButton.setEnabled(True)
        window.catName.setEnabled(True)
        window.editCategories.setText("Edit categories")
        save_categories()

    count = window.Categories.count()
    for i in range(count):
        window.Categories.itemAt(i).toggle_edit_mode()


def _on_color_pressed():
    '''
    Opens a color selection window
    '''
    color = QColorDialog.getColor()
    editor._on_color_selected(color)


def _on_name_changed():
    '''
    Called when the user hits enter when editing a name
    Will release the focus from the line edit
    '''
    window.nameEdit.clearFocus()


def _on_help_clicked():
    '''
    Opens the help window
    '''
    curr_dir = os.getcwd()
    help_file = os.path.join(curr_dir, "data", "ui", "help", "help.html")
    webbrowser.open(help_file)


def _on_redo_clicked():
    '''
    [Not implemented yet]
    '''
    print("Redo pressed")


def _on_undo_clicked():
    '''
    [Not implemented yet]
    '''
    print("Undo pressed")


def _on_settings_clicked():
    '''
    Opens the settings window
    '''
    Settings.show()


def _on_save_as_clicked():
    '''
    Called when 'save as' is pressed
    '''
    print("Save as pressed")
    Loading.save_to_json_as(editor, image, editor.annotation_data)


def _on_load_clicked():
    '''
    Called when 'load' is pressed
    '''
    print("Load pressed")
    global image
    image = Loading.load_json(editor)


def _on_save_clicked():
    '''
    Called when 'save' is pressed
    '''
    print("Save pressed")
    Loading.save_to_json(editor, image, editor.annotation_data)


def _on_export_image_clicked():
    '''
    Called when 'Export as JPEG' is pressed
    '''
    print("Export image pressed")
    Loading.export_image_with_data(editor)


def _on_export_coco_clicked():
    '''
    Called when 'Export as COCO' is pressed
    '''
    print("Export coco pressed")
    Loading.export_as_coco(editor, image, editor.annotation_data)


def _on_export_xml_clicked():
    '''
    Called when 'Export as XML is pressed'
    '''
    print("Export XML pressed")
    Loading.export_as_xml(editor, image)


def _on_delete_annotation():
    '''
    Called when 'delete annotation' is pressed
    Will prompt if the user is sure about it
    '''
    print("Delete annotation")
    ans = popup_yes_no("Question", "Confirm",
                       "Are you sure you want to delete this annotation")
    if ans == AnswerType.Yes:
        toDelete = editor.get_current_annotation()
        editor.delete_annotation(toDelete)
    elif ans == AnswerType.No:
        return


def _on_delete_all():
    '''
    Called when 'delete all annotations' is pressed
    Will prompt if the user is sure about it
    '''
    ans = popup_yes_no("Question", "Confirm",
                       '''
Are you sure you want to delete the selected annotations?
                       ''')
    if ans == AnswerType.Yes:
        editor.delete_selected_annotations()
    elif ans == AnswerType.No:
        return


def _on_tool_select(checked):
    '''
    Selects the select tool
    '''
    editor.set_mode(Mode.SELECT)
    window.actionCreate.setChecked(False)
    window.actionCreatePolygon.setChecked(False)
    window.actionPan.setChecked(False)
    window.actionEdit.setChecked(False)

    # Prevents user from deselecting the tool
    if not checked:
        window.actionSelect.setChecked(True)


def _on_tool_create(checked):
    '''
    Selects the create tool
    '''
    editor.set_mode(Mode.CREATE)
    window.actionSelect.setChecked(False)
    window.actionCreatePolygon.setChecked(False)
    window.actionPan.setChecked(False)
    window.actionEdit.setChecked(False)

    # Prevents user from deselecting the tool
    if not checked:
        window.actionCreate.setChecked(True)


def _on_tool_edit(checked):
    '''
    Selects the edit tool
    [Not implemented yet]
    '''
    editor.set_mode(Mode.EDIT)
    window.actionSelect.setChecked(False)
    window.actionCreatePolygon.setChecked(False)
    window.actionCreate.setChecked(False)
    window.actionPan.setChecked(False)

    # Prevents user from deselecting the tool
    if not checked:
        window.actionEdit.setChecked(True)


def _on_tool_pan(checked):
    '''
    Selects the pan tool
    '''
    editor.set_mode(Mode.PAN)
    window.actionSelect.setChecked(False)
    window.actionCreatePolygon.setChecked(False)
    window.actionCreate.setChecked(False)
    window.actionEdit.setChecked(False)

    # Prevents user from deselecting the tool
    if not checked:
        window.actionPan.setChecked(True)


def _on_tool_create_polygon(checked):
    '''
    Selects the create polygon tool
    '''
    editor.set_mode(Mode.POLYGON)
    window.actionSelect.setChecked(False)
    window.actionCreate.setChecked(False)
    window.actionPan.setChecked(False)
    window.actionEdit.setChecked(False)

    # Prevents user from deselecting the tool
    if not checked:
        window.actionCreatePolygon.setChecked(True)


def _on_import_clicked(pressed):
    '''
    Called when 'import image' is pressed
    Opens a folder dialog, allowing the user to select and image to load
    '''
    global image
    global imagePath

    file = QFileDialog.getOpenFileName(
        window, "open", Settings.import_folder,
        "Images (*.jpg *.jpeg *.png *.bmp *.gif)")

    pixmap = QPixmap(file[0])
    if pixmap.isNull():
        return

    # reset annotation list
    editor.reset_annotations()
    window.setWindowTitle("Visionator - Unsaved")
    Loading.loaded_file = "Unsaved"

    # Copy loaded file into image folder for easy access when loading
    # Also prevents erros when trying to load a deleted/moved image
    imageDir = Settings.IMAGE_DIR

    # Make sure image folder exists then copy
    if not os.path.exists(imageDir):
        os.makedirs(imageDir)
    head, tail = os.path.split(file[0])
    imagePath = os.path.join(imageDir, tail)
    shutil.copyfile(file[0], imagePath)
    image = tail

    # Add image to the scene
    editor.scene().addPixmap(pixmap)
    editor.scene().setSceneRect(pixmap.rect())
