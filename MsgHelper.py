from PySide6.QtWidgets import QMessageBox


class AnswerType(QMessageBox):
    '''
    Class that is for handling answer types
    '''

    def __init__(self):
        super().__init__()


class Popup(QMessageBox):
    '''
    Base class for the 'custom' popups
    '''
    type = ""
    title = ""
    message = ""

    def __init__(self, type, title, message):
        super().__init__()
        self.type = type
        self.title = title
        self.message = message


class OkPopup(Popup):
    '''
    Popup that only has "Ok" as an answer
    '''

    def __init__(self, type, title, message):
        super().__init__(type, title, message)

    def show(self):
        answer = None
        match self.type:
            case "Question":
                answer = self.question(self, self.title, self.message,
                                       Popup.Ok)
            case "Information":
                answer = self.information(self, self.title, self.message,
                                          Popup.Ok)
            case "Warning":
                answer = self.warning(self, self.title, self.message,
                                      Popup.Ok)
            case "Critical":
                answer = self.critical(self, self.title, self.message,
                                       Popup.Ok)
        return answer


class YesNoPopup(Popup):
    '''
    Popup that has "Yes" and "No" as answers
    '''

    def __init__(self, type, title, message):
        super().__init__(type, title, message)

    def show(self):
        answer = None
        match self.type:
            case "Question":
                answer = self.question(self, self.title, self.message,
                                       Popup.Yes, Popup.No)
            case "Information":
                answer = self.information(self, self.title, self.message,
                                          Popup.Yes, Popup.No)
            case "Warning":
                answer = self.warning(self, self.title, self.message,
                                      Popup.Yes, Popup.No)
            case "Critical":
                answer = self.critical(self, self.title, self.message,
                                       Popup.Yes, Popup.No)
        return answer


class OkCancelPopup(Popup):
    '''
    Popup that has "Ok" and "Cancel" as answers
    '''

    def __init__(self, type, title, message):
        super().__init__(type, title, message)

    def show(self):
        answer = None
        match self.type:
            case "Question":
                answer = self.question(self, self.title, self.message,
                                       Popup.Cancel, Popup.Ok)
            case "Information":
                answer = self.information(self, self.title, self.message,
                                          Popup.Cancel, Popup.Ok)
            case "Warning":
                answer = self.warning(self, self.title, self.message,
                                      Popup.Cancel, Popup.Ok)
            case "Critical":
                answer = self.critical(self, self.title, self.message,
                                       Popup.Cancel, Popup.Ok)
        return answer


# The following methods are meant to be called.
def popup_ok(type, title, message):
    """
    type: string -> 'Question', 'Information', 'Warning', 'Critical'
    title: string
    message: string

    Will give 'Ok' as possible answers
    """
    msgBox = OkPopup(type, title, message)
    return msgBox.show()


def popup_yes_no(type, title, message):
    """
    type: string -> 'Question', 'Information', 'Warning', 'Critical'
    title: string
    message: string

    Will give 'Yes' or 'No' as possible answers
    """
    msgBox = YesNoPopup(type, title, message)
    return msgBox.show()


def popup_ok_cancel(type, title, message):
    """
    type: string -> 'Question', 'Information', 'Warning', 'Critical'
    title: string
    message: string

    Will give 'Yes' or 'No' as possible answers
    """
    msgBox = OkCancelPopup(type, title, message)
    return msgBox.show()
