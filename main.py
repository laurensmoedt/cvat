import sys
import Events
import Settings

from PySide6.QtWidgets import QApplication
from PySide6.QtGui import QIcon
from Loading import load_ui_file
from Editor import Editor
from Category import load_categories


def InitUI():
    '''
    Initializes the app
    '''
    app = QApplication(sys.argv)

    # Get the ui and set the icon
    window = load_ui_file("mainwindow.ui")
    window.setWindowIcon(QIcon("icon.png"))

    # Show the window
    window.showMaximized()

    # Store the window and the editor
    Events.window = window
    new_editor = Editor(window)
    Events.editor = new_editor

    # Add the editor to the window
    window.CenterWindow.addWidget(new_editor)

    # Properly set the tabs
    window.tabifyDockWidget(window.CategoryDock, window.PropertiesDock)

    # Hide the property windows, as nothing is selected on boot
    window.Properties.hide()
    window.multipleProperties.hide()

    # Connect all button signals, initialize settings and the categories
    Events.connect_signals()
    Settings.init()
    categories = load_categories(window)
    new_editor.categories = categories
    new_editor.settings_values = False

    sys.exit(app.exec())


if __name__ == "__main__":
    InitUI()
