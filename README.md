# Download
Visionator can be downloaded for Windows and Linux in the [releases](https://gitlab.com/laurensmoedt/cvat/-/releases) section.
MacOS users are going to have to build the application themselves. Follow the steps below.

# How to build Visionator

**Required packages:**

- `pip install pyinstaller`
- `pip install opencv-python`
- `pip install PySide6`

For the dark themes to work, they also need to be installed with pip:

- `pip install qdarkstyle`
- `pip install pyqtdarktheme`

## Automatic build

Run `build.sh` from the command line.
Optional arguments:

- `-c` performs a clean build. This will clear out any settings or categories previously set _from the dist folder only_. Using this option ensures a clean start for new users.
- `-d` creates a debug build. This build will open a console as well as the application.
- `-h` shows a help dialog

The build can be then be found in the `dist` folder.

## Manual build
To build the program manually, follow these steps:

1. If Python is not installed, install python [here](https://www.python.org/downloads/)
2. Open the terminal in the folder that contains the `main.py` script.
3. Run `pip install pyinstaller` if it's not installed already
4. Run `pyinstaller --onefile --windowed --name="Visionator" --icon="visionator.ico" main.py` (this is untested as of now)
5. Copy the data folder into the same folder as the executable (usually the new 'dist' folder).
   If it's for testing, that's it! If it's intended to be distributed, it's advised to do step 6:
6. Inside the 'data' folder, delete the following items:
   - 'images' folder
   - 'settings.cfg' file
   - 'categories' file (OPTIONAL), if you wish to share the categories that you've created, don't delete this file. Users will still be able
     to modify it.
     Deleting these ensures a clean start for new users.
