import os
# import json
import Loading
from enum import Enum
from Polygon import Polygon
from PySide6.QtGui import (QMouseEvent, QPen, QBrush, QGuiApplication,
                           QPolygonF, QColor, QPixmap, QFont, QFontMetrics)
from PySide6.QtCore import Signal, QPointF, Qt, QRect
from PySide6.QtWidgets import (QGraphicsView, QGraphicsScene,
                               QGraphicsRectItem, QGraphicsPolygonItem,
                               QGraphicsSimpleTextItem)

# Some constants
ALPHA_VALUE = 25  # Alpha value for the fill effect - range(0, 255)
POINT_MARGIN = 2  # How close a new point should be to the first
# to close the current polygon in pixels
DEFAULT_RECT_NAME = "new_rectangle"
DEFAULT_POLYGON_NAME = "new_polygon"

# The currently selected annotation
# This is a dictionary containing all data about the annotation
selected_annotation = None


class Mode(Enum):
    '''
    Enum representing tool modes
    '''
    SELECT = 0
    PAN = 1
    CREATE = 2
    EDIT = 3
    DELETE = 4  # Unused
    POLYGON = 5


def is_point_in_margin(a, b):
    '''
    Checks if point A is close to point B,
    by a margin of the constant POINT_MARGIN
    '''

    diff_x = a.x() - b.x()
    diff_y = a.y() - b.y()

    if diff_x > POINT_MARGIN or diff_x < -POINT_MARGIN:
        return False

    if diff_y > POINT_MARGIN or diff_y < -POINT_MARGIN:
        return False

    return True


def has_bbox_child(object):
    '''
    object: QGraphpicsItem to check on

    Checks if object has a rectangle(bounding box) as a child
    '''
    for child in object.childItems():
        if type(child) is QGraphicsRectItem:
            return True
    return False


def get_bbox_child(object):
    '''
    object: QGraphpicsItem to check on

    Checks if object has a rectangle(bounding box) as a child

    Returns the bounding box child
    '''
    for child in object.childItems():
        if type(child) is QGraphicsRectItem:
            return child
    return None


class Editor(QGraphicsView):
    '''
    Editor class
        The central widget containing the image
    '''
    annotation_data = []  # Contains dictionaries with data of an annotation
    annotations = []  # Contains references to annotation items on screen
    categories = []  # List of available categories
    selected_annotations = []  # List of selected annotations

    index = None  # Index of current annotation, relating to the 3 lists above
    window = None  # Reference to the application window
    mode = Mode.SELECT  # Current tool mode

    # Some useful variables when creating or editing annotations
    start = QPointF()  # Keeps track of rect start
    end = QPointF()  # Keeps track of rect end
    rect = None  # Temp reference to current rect
    polygon = None  # Temp reference to current polygon

    # Variables for keeping track about what is currently happening
    deleting_items = False
    panning = False  # Whether the user is panning or not
    creating_rect = False  # If the a rect is being created
    setting_values = True  # If the backend is setting values in the property
    # window or not, to prevent unwanted signals
    # Starts enabled to prevent settings at boot triggering methods
    selected_with_mouse = False

    # Signal for when the mouse moves in the editor window
    mouseMoved = Signal(QPointF)

    def __init__(self, window):
        self.window = window
        super().__init__()
        scene = QGraphicsScene()
        self.setScene(scene)
        self.setObjectName("Editor")
        self.setMouseTracking(True)
        self.setDragMode(QGraphicsView.RubberBandDrag)

        self.window.Outliner.itemSelectionChanged.connect(
            self._outliner_selection_changed)

    def mousePressEvent(self, event):
        if Loading.loaded_file == "":
            return

        # Enable Middle Mouse Button panning
        if event.button() == Qt.MiddleButton:
            self.setDragMode(QGraphicsView.ScrollHandDrag)
            self.panning = True
            mouse_event = QMouseEvent(QMouseEvent.MouseButtonPress,
                                      event.localPos(), event.screenPos(),
                                      Qt.LeftButton, Qt.LeftButton,
                                      Qt.NoModifier)
            return super().mousePressEvent(mouse_event)

        # Return if it's not a LMB or if we're panning
        if event.button() != Qt.LeftButton:
            return

        if self.panning:
            return

        # Create a new polygon annotation
        if self.mode == Mode.POLYGON:
            pos = self.mapToScene(event.pos())
            self.set_polygon(pos)

        # Create a new rectangle annotation
        if self.mode == Mode.CREATE:
            self.begin = self.mapToScene(event.pos())
            self.rect = self.scene().addRect(self.begin.x(), self.begin.y(),
                                             0, 0)
            self.creating_rect = True
        else:
            super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        if Loading.loaded_file == "":
            return

        # Return a new mouse release event if we're panning
        if self.panning:
            self.panning = False
            if self.mode != Mode.PAN:
                if self.mode == Mode.SELECT:
                    self.setDragMode(QGraphicsView.RubberBandDrag)
                else:
                    self.setDragMode(QGraphicsView.NoDrag)
            mouse_event = QMouseEvent(QMouseEvent.MouseButtonRelease,
                                      event.localPos(), event.screenPos(),
                                      Qt.LeftButton, Qt.LeftButton,
                                      Qt.NoModifier)
            return super().mouseReleaseEvent(mouse_event)

        if self.mode == Mode.SELECT and event.button() == Qt.LeftButton:
            self.select_objects(event)

        # Create a new annotation if the size is bigger than 0
        if self.mode == Mode.CREATE and not self.panning:
            r = self.rect.rect()
            if r.width() == 0 and r.height() == 0:
                self.scene().removeItem(self.rect)
            else:
                self.create_rect()
        else:
            super().mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        if Loading.loaded_file == "":
            return

        # Display coords in the status bar at the bottom
        pos = self.mapToScene(event.pos())
        self.window.statusBar().showMessage(
            f"({round(pos.x())}, {round(pos.y())})")

        # Set the rectangle to the new size. width and height == end - start
        if self.mode == Mode.CREATE and self.creating_rect:
            self.end = self.mapToScene(event.pos())
            pen = QPen(Qt.blue, 1, Qt.SolidLine,
                       Qt.SquareCap, Qt.RoundJoin)
            self.rect.setPen(pen)
            self.rect.setRect(self.begin.x(), self.begin.y(),  # x, y
                              (self.end.x() - self.begin.x()),  # width
                              (self.end.y() - self.begin.y()))  # height

        return super().mouseMoveEvent(event)

    def wheelEvent(self, event):
        if self.scene() is None:
            return

        # Get the scroll amount
        delta = event.angleDelta().y()

        # Get the active keyboard modifiers
        modifiers = QGuiApplication.keyboardModifiers()

        # If CONTROL is held, zoom in or out
        if modifiers == Qt.ControlModifier:
            zoom_x = self.transform().m11()
            # zoom_y = self.transform().m22()
            if delta > 0:
                scale = 1.25
                if zoom_x > 15:
                    scale = 1

            if delta < 0:
                scale = .8
                if zoom_x < 0.05:
                    scale = 1
            self.scale(scale, scale)
            return

        # If shift is held, scroll left or right
        if modifiers == Qt.ShiftModifier:
            x = self.horizontalScrollBar().value()
            self.horizontalScrollBar().setValue(x - delta)

        # If no modifiers scroll up or down
        else:
            y = self.verticalScrollBar().value()
            self.verticalScrollBar().setValue(y - delta)

    def select_objects(self, event):
        '''
        event: QMouseEvent of the mouse click

        Handles selecting objects on screen when using the select tool
        '''
        self.outliner_deselect_all()
        self.selected_annotations = []  # Reset the list
        polygonRect = self.mapToScene(self.rubberBandRect())
        sr = polygonRect.boundingRect()  # Convert QRectF to QRect
        selectRect = QRect(sr.x(), sr.y(), sr.width(), sr.height())
        selected = self.scene().items(selectRect)

        # If the rect is not valid, it means it was a single click
        # We then check if there's an item at the point of a click
        if not selectRect.isValid():
            pos = self.mapToScene(event.pos())  # Get correct position
            for annotation in self.annotations:
                bbox = annotation.boundingRect()
                if bbox.contains(pos):
                    self.selected_annotations.append(annotation)
                    break
        else:
            for obj in selected:  # Loop through items that intersected
                t = type(obj)

                # If it's a rect or polygon
                if t is QGraphicsRectItem or t is QGraphicsPolygonItem:
                    r = obj.boundingRect()  # Convert QRectF to QRect
                    obj_rect = QRect(r.x(), r.y(), r.width(), r.height())

                    # Make sure we only get objects FULLY inside the selection
                    if selectRect.contains(obj_rect, True):

                        # Make sure it's not a polygon bounding box
                        if not type(obj.parentItem()) is QGraphicsPolygonItem:
                            self.selected_annotations.append(obj)

        self.selected_with_mouse = True
        self.update_outliner()

    def update_outliner(self):
        '''
        Sets the outliner items to the selection
        This will call the SelectionChanged signal on the outliner
        '''
        if len(self.selected_annotations) == 0:
            return

        # Find selected objects corresponding outliner item and select those
        for ann in self.selected_annotations:
            i = self.annotations.index(ann)
            item = self.window.Outliner.item(i)
            item.setSelected(True)

    def _outliner_selection_changed(self):
        '''
        Gets called when the selection on the outliner is changed
        Will set the properties window according to selection
        '''
        if self.deleting_items:
            return

        selected = self.window.Outliner.selectedItems()
        size = len(selected)

        # If none selected just display nothing
        if size == 0:
            self.window.Properties.hide()
            self.window.multipleProperties.hide()
            return

        # If one is selected show the properties
        if size == 1:
            self.window.Properties.show()
            self.window.multipleProperties.hide()

            i = self.window.Outliner.row(selected[0])
            self.update_properties(i)
            ann = self.annotations[i]
            self.centerOn(ann)  # Pan view to the annotation

            # Display corresponding properties for either rectangle or polygon
            if type(ann) is QGraphicsRectItem:
                self.window.multipleProperties.hide()
                self.show_rect_properties()
            elif type(ann) is QGraphicsPolygonItem:
                self.window.multipleProperties.hide()
                self.show_polygon_properties()

            return

        # If it's more than one, fill them in with their color
        for item in selected:
            i = self.window.Outliner.row(item)
            ann = self.annotations[i]

            # Make sure not to double select them if they're selected with
            # the select tool.
            if not self.selected_with_mouse:
                self.selected_annotations.append(ann)

            color = QColor(self.annotation_data[i]["color"])
            color.setAlpha(ALPHA_VALUE)
            ann.setBrush(QBrush(color))

        self.window.Properties.hide()
        self.window.multipleProperties.show()
        self.selected_with_mouse = False

    def update_properties(self, index):
        '''
        index: integer of the annotations' index

        This updates the property window to display the properties
        of the annotation at the given index
        '''
        if self.deleting_items:
            return

        # Set the fill to empty of the main objects (not the name labels)
        for obj in self.scene().items():
            if type(obj) is QGraphicsRectItem:
                obj.setBrush(QBrush(QColor(0, 0, 0, 0)))
            elif type(obj) is QGraphicsPolygonItem:
                obj.setBrush(QBrush(QColor(0, 0, 0, 0)))

        # Make sure the signal receiving methods know we're
        # prepping the property window
        self.setting_values = True

        # Set the global index to this annotation
        self.index = index

        # Set the current annotation
        global selected_annotation
        selected_annotation = self.annotation_data[self.index]

        # Fill in all the property editors with the corresponding data
        self.window.nameEdit.setText(selected_annotation["name"])
        self.window.catEdit.setCurrentIndex(
            self.window.catEdit.findText(selected_annotation["category"]))
        self.window.xEdit.setValue(selected_annotation["x"])
        self.window.yEdit.setValue(selected_annotation["y"])
        self.window.widthEdit.setValue(selected_annotation["width"])
        self.window.heightEdit.setValue(selected_annotation["height"])
        color = QColor(selected_annotation["color"])
        self.window.colorEdit.setStyleSheet(
            f"background-color: {color.name()}")

        # Set the selected annotations fill to its translucent color
        ann = self.get_current_annotation()
        color.setAlpha(ALPHA_VALUE)
        ann.setBrush(QBrush(color))

        # If it has a rectangle child, set the property for its visibility
        bbox = get_bbox_child(ann)
        if bbox is not None:
            vis = bbox.isVisible()
            self.window.polyBoxShow.setChecked(vis)

        # Make sure the signal receiving methods know we're
        # done prepping the property window
        self.setting_values = False

    def show_polygon_properties(self):
        '''
        Shows just the properties for polygon annotations
        '''
        self.window.polyBoxShow.show()
        self.window.xEdit.hide()
        self.window.xLabel.hide()
        self.window.yEdit.hide()
        self.window.yLabel.hide()
        self.window.widthEdit.hide()
        self.window.widthLabel.hide()
        self.window.heightEdit.hide()
        self.window.heightLabel.hide()

    def show_rect_properties(self):
        '''
        Shows all properties for rectangle annotations
        '''
        self.window.polyBoxShow.hide()
        self.window.xEdit.show()
        self.window.xLabel.show()
        self.window.yEdit.show()
        self.window.yLabel.show()
        self.window.widthEdit.show()
        self.window.widthLabel.show()
        self.window.heightEdit.show()
        self.window.heightLabel.show()

    def set_mode(self, mode):
        '''
        Sets the tool mode to the given tool
        mode: Mode enum
        '''
        self.mode = mode
        self.setDragMode(QGraphicsView.NoDrag)
        match mode:
            case Mode.SELECT:
                print("Set mode: Select")
                self.setDragMode(QGraphicsView.RubberBandDrag)
            case Mode.PAN:
                print("Set mode: Pan")
                self.setDragMode(QGraphicsView.ScrollHandDrag)
            case Mode.CREATE:
                print("Set mode: Create rect")
            case Mode.EDIT:
                print("Set mode: Edit")
            case Mode.DELETE:
                print("Set mode: Delete")
            case Mode.POLYGON:
                print("Set mode: Create polygon")

    def set_polygon(self, pos):
        if self.polygon is None:
            # Create a new polygon if we're not editing one right now
            self.polygon = Polygon(pos)

            # Create a path to display the line
            path = self.scene().addPath(self.polygon.path)

            # Circle to mark our starting point
            ellipse = self.scene().addEllipse(pos.x(), pos.y(), 1, 1)

            # Set the color of both the line and circle to blue
            path.setPen(QPen(Qt.blue))
            ellipse.setPen(QPen(Qt.blue))

            # Give the references to the path and circle to the new polygon
            self.polygon.path_ref = path
            self.polygon.start_ref = ellipse
            return

        # If it's not a new polygon, simply add a new point to it
        closed = self.polygon.add_point(pos)

        # If the new point is within the margin of the first point, it
        # will be marked as closed
        if closed:
            # Remove the path and circle from the screen
            self.scene().removeItem(self.polygon.path_ref)
            self.scene().removeItem(self.polygon.start_ref)

            # Add a proper polygon item in their place
            ann = self.scene().addPolygon(self.polygon.polygon)
            self.create_polygon(ann)

            # Set the tool to select and reset the polygon value
            self.window.actionSelect.trigger()
            self.polygon = None

    def create_polygon(self, ref):
        '''
        ref: reference to the polygon item on screen

        Generates a new polygon item and data for it
        '''
        self.outliner_deselect_all()

        # Create new polygon data
        new_annotation = {
            "name": DEFAULT_POLYGON_NAME,
            "x": 0,
            "y": 0,
            "width": 0,
            "height": 0,
            "color": QColor(0, 0, 0).name(),
            "category": "",
            "polygon": self.polygon.points
        }
        # Store the annotation references in lists for easy access later
        self.annotations.append(ref)
        self.annotation_data.append(new_annotation)

        self.index = len(self.annotations) - 1

        # Add the annotation to the outliner and select it
        self.window.Outliner.addItem(DEFAULT_POLYGON_NAME)
        self.window.Outliner.setCurrentRow(self.index)

        # Create a bounding box for the polygon
        self.create_poly_bbox(ref)

    def create_poly_bbox(self, ref):
        bbox = None
        rect = self.get_current_annotation().polygon().boundingRect()
        bbox = QGraphicsRectItem(rect, parent=ref)

        pen = QPen(Qt.black, 1, Qt.SolidLine,
                   Qt.SquareCap, Qt.RoundJoin)
        bbox.setPen(pen)

        # Store the data in the annotation data
        selected_annotation["x"] = bbox.rect().x()
        selected_annotation["y"] = bbox.rect().y()
        selected_annotation["width"] = bbox.rect().width()
        selected_annotation["height"] = bbox.rect().height()

        # Creat the name label
        self.create_annotation_label(
            DEFAULT_POLYGON_NAME,
            bbox.rect().x(), bbox.rect().y(), ref)

    def create_rect(self):
        '''
        Creates a rectangle from self.rect and stores it
        '''
        self.outliner_deselect_all()
        self.window.actionSelect.trigger()

        # Reset some values
        self.creating_rect = False
        self.begin = QPointF(0, 0)
        self.end = QPointF(0, 0)

        # Set the color
        pen = QPen(Qt.black, 1, Qt.SolidLine,
                   Qt.SquareCap, Qt.RoundJoin)
        self.rect.setPen(pen)

        rect = self.rect.rect()

        # Generate rectangle data
        new_annotation = {
            "name": DEFAULT_RECT_NAME,
            "x": rect.x(),
            "y": rect.y(),
            "width": rect.width(),
            "height": rect.height(),
            "color": QColor(0, 0, 0).name(),
            "category": "",
        }

        # Store the annotation references in lists for easy access later
        self.annotations.append(self.rect)
        self.annotation_data.append(new_annotation)

        self.index = len(self.annotations) - 1

        # Add the annotation to the outliner and select it
        self.window.Outliner.addItem(DEFAULT_RECT_NAME)
        self.window.Outliner.setCurrentRow(self.index)

        self.create_annotation_label(
            DEFAULT_RECT_NAME, rect.x(), rect.y(), self.rect)

    def draw_all_annotations(self, data):
        '''
        data: dictionary containing all save data

        Draws all annotations from the given data
        '''
        # Get the image
        img = QPixmap(os.path.join("data", "images",
                                   data["images"][0]["file_name"]))
        self.scene().addPixmap(img)

        # Loop through all stored annotations
        for annotation in data['annotations']:
            self.annotation_data.append(annotation)
            self.window.Outliner.addItem(annotation["name"])

            # If the annotation is a polygon, create a new polygon
            if "polygon" in annotation:
                polygon = QPolygonF()

                # Add all points to the polygon
                for point in annotation["polygon"]:  # [[x, y], [x, y],..]
                    p = QPointF(point[0], point[1])  # [x, y]
                    polygon.append(p)

                # Add the polygon to the scene and color it
                pol = self.scene().addPolygon(polygon)
                pol.setPen(QPen(annotation["color"]))

                # Add the bounding box
                rect = pol.polygon().boundingRect()
                QGraphicsRectItem(
                    rect, parent=pol)

                # Add it to the annotations list and create the name label
                self.annotations.append(pol)
                self.create_annotation_label(
                    annotation["name"], annotation["x"],
                    annotation["y"], pol)

            # If it's not a polygon it's a rectangle
            else:
                # Create the rect
                self.rect = self.scene().addRect(
                    annotation["x"], annotation["y"],
                    annotation["width"], annotation["height"])

                # Add it to the annotations list and create the name label
                self.annotations.append(self.rect)
                self.rect.setPen(QPen(annotation["color"]))
                self.create_annotation_label(
                    annotation["name"], annotation["x"],
                    annotation["y"], self.rect)

    def delete_annotation(self, to_delete):
        '''
        Deletes the given annotation
        to_delete: QGraphicsItem that needs to be deleted
        '''
        # Get the index
        index = self.annotations.index(to_delete)

        # Remove it from all lists and the scene
        self.annotation_data.pop(index)
        self.annotations.pop(index)
        self.window.Outliner.takeItem(index)
        self.scene().removeItem(to_delete)

        # Deselect everything and hide properties
        self.outliner_deselect_all()
        self.window.Properties.hide()
        self.window.multipleProperties.hide()

    def delete_selected_annotations(self):
        '''
        Deletes the annotations in the selected_annotations list
        '''
        self.deleting_items = True
        for annotation in self.selected_annotations:
            self.delete_annotation(annotation)

        self.selected_annotations = []
        self.deleting_items = False

    def set_new_rect(self, rect):
        '''
        rect: QRectF that needs to be modified

        Sets the new size to the given rectangle
        '''
        x = selected_annotation["x"]
        y = selected_annotation["y"]
        width = selected_annotation["width"]
        height = selected_annotation["height"]

        # Make sure the rect stays on the image
        if x < 0:
            x = 0
            selected_annotation["x"] = 0
        if y < 0:
            y = 0
            selected_annotation["y"] = 0

        rect.setRect(x, y, width, height)

    def _on_name_changed(self, new_name):
        '''
        new_name: String of the new name

        Gets called when the user changes the name of an annotation
        '''
        if self.setting_values:
            return

        selected_annotation["name"] = new_name
        item = self.window.Outliner.currentItem()
        item.setText(new_name)

        rectItem = self.get_current_annotation()
        self.set_annotation_label(rectItem)

    def _on_category_changed(self, category):
        '''
        category: String name of the new category

        Gets called when the user changes the category of an annotation
        '''
        if self.setting_values:
            return
        selected_annotation["category"] = category

    def _on_x_changed(self, value):
        '''
        value: Float value of the X position

        Gets called when the user changes the X value of an annotation
        '''
        if self.setting_values:
            return

        rect = self.get_current_annotation()
        selected_annotation["x"] = value
        self.set_new_rect(rect)
        self.set_annotation_label_pos()

    def _on_y_changed(self, value):
        '''
        value: Float value of the Y position

        Gets called when the user changes the Y value of an annotation
        '''
        if self.setting_values:
            return

        rect = self.get_current_annotation()
        selected_annotation["y"] = value
        self.set_new_rect(rect)
        self.set_annotation_label_pos()

    def _on_width_changed(self, value):
        '''
        value: Float value of the width

        Gets called when the user changes the width of an annotation
        '''
        if self.setting_values:
            return

        rect = self.get_current_annotation()
        selected_annotation["width"] = value
        self.set_new_rect(rect)

    def _on_height_changed(self, value):
        '''
        value: Float value of the height

        Gets called when the user changes the height of an annotation
        '''
        if self.setting_values:
            return

        rect = self.get_current_annotation()
        selected_annotation["height"] = value
        self.set_new_rect(rect)

    def _on_color_selected(self, color):
        '''
        color: QColor of the selected color

        Gets called when the user changes the color of an annotation
        '''
        if self.setting_values:
            return

        # Set the background of the color select button
        self.window.colorEdit.setStyleSheet(
            f"background-color: {color.name()}")

        selected_annotation["color"] = color.name()
        rect = self.get_current_annotation()
        pen = QPen(color, 1, Qt.SolidLine,
                   Qt.SquareCap, Qt.RoundJoin)
        rect.setPen(pen)
        color.setAlpha(ALPHA_VALUE)
        rect.setBrush(QBrush(color))

    def get_current_annotation(self):
        '''
        Returns the selected annotation
        '''
        return self.annotations[self.index]

    def reset_annotations(self):
        '''
        Removes all annotations from the canvas
        '''
        self.annotation_data = []
        self.annotations = []
        self.window.Outliner.clear()
        for item in self.scene().items():
            self.scene().removeItem(item)

    def create_annotation_label(self, name, x, y, ann):
        '''
        name: string value
        x: float x value of the annotation
        y: float y value of the annotation
        ann: QGraphicsItem that will be set as the labels parent

        Creates a new name label for an annotation
        '''
        font = QFont()
        font.setPointSize(12)
        text = QGraphicsSimpleTextItem(name, parent=ann)
        text.setFont(font)
        pen = QPen()
        text.setPen(pen)
        text.setPos(x, y - QFontMetrics(text.font()).height())

    def set_annotation_label(self, annotation):
        '''
        annotation: QGraphicsItem of the annotation

        Sets the name to the new name
        '''
        for child in annotation.childItems():
            if type(child) is QGraphicsSimpleTextItem:
                child.setText(selected_annotation["name"])

    def set_annotation_label_pos(self):
        '''
        Sets the position of the label of the current annotation
        '''
        x = selected_annotation["x"]
        y = selected_annotation["y"]

        for child in self.get_current_annotation().childItems():
            if type(child) is QGraphicsSimpleTextItem:
                child.setPos(x, y - QFontMetrics(child.font()).height())

    def outliner_deselect_all(self):
        '''
        Deselects all items in the outliner and resets the fill color
        '''
        for item in self.window.Outliner.selectedItems():
            item.setSelected(False)

        for obj in self.scene().items():
            if type(obj) is QGraphicsRectItem:
                obj.setBrush(QBrush(QColor(0, 0, 0, 0)))
            elif type(obj) is QGraphicsPolygonItem:
                obj.setBrush(QBrush(QColor(0, 0, 0, 0)))

    def _on_poly_box_show_toggled(self, toggled):
        '''
        Gets called when user toggles the bounding box visibility of a polygon
        '''
        bbox = get_bbox_child(self.get_current_annotation())
        bbox.setVisible(toggled)
