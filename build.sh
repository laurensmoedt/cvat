#!/bin/bash

usage() { echo "Usage: $0 [-h, -c, -d]" 1>&2; read -p "Press any key to exit..."; exit 0; }
print_help() { echo "-h: displays this";echo "-c: performs a clean build"; echo "-d perform a debug build. This build opens a console next to the main program" 1>&2; read -p "Press any key to exit..."; exit 0;}

debug=0
clean=0

build() {
	if [ $debug == 1 ] ; then
		pyinstaller --onefile --windowed --name="Visionator" --icon="visionator.ico" main.py
	else
		pyinstaller --onefile --windowed --noconsole --name="Visionator" --icon="visionator.ico" main.py
	fi

	cp -R data dist
	cp icon.png dist
}

clean_build() {
	build

	for f in $(cat build_exclude_files.txt) ; do
		rm -R "dist/data/$f"
	done
}

while getopts "cdh" o; do
	case "${o}" in
		c)
			clean=1
			;;
		d)
			debug=1
			;;
		h)
			print_help
			;;
		*)
			usage
			;;
	esac
done

if [ $clean == 1 ] ; then
	clean_build
else
	build
fi


