from PySide6.QtGui import QPainterPath
from PySide6.QtCore import QPoint
from MsgHelper import popup_ok
# from PySide6.QtWidgets import QGraphicsPolygonItem

POINT_MARGIN = 2  # How close a new point should be to the first
# to close the current polygon in pixels


class Polygon():
    '''
    Class that helps with creating polygons
    '''
    polygon = None  # Actual polygon
    path = None  # Path when polygon is still being created
    first = None  # First point of the polygon

    path_ref = None  # Reference to the on-screen path item
    start_ref = None  # Reference to the on-screen circle item

    points = []  # Array with coords of every point, stored like this:
    # [[x, y], [x, y], [x, y], [x, y], [x, y], ...]

    def __init__(self, position):
        '''
        position: Start position of the polygon
        '''
        # Convert to QPoint from QPointF
        pos = QPoint(position.x(), position.y())

        # Create the path
        self.path = QPainterPath(pos)

        # Store the position
        self.first = pos
        self.points.append([pos.x(), pos.y()])

    def add_point(self, point):
        '''
        point: Position to be added to the polygon

        Adds a point to this polygon
        '''
        # If the polygon is larger than 1, we check if it's close to the first
        # point in the polygon
        if len(self.path.toFillPolygon()) > 1:
            if is_point_in_margin(point, self.first):
                # Clamp the point to the first for a perfect closing
                point = self.first

                # Conver path to a polygon
                self.polygon = self.path.toFillPolygon()

                # Add the last point to the list and notify user
                self.points.append([point.x(), point.y()])
                send_popup()

                # Return True to indicate the polygon has been closed
                return True

        # Add point to the list
        self.points.append([point.x(), point.y()])

        # Draw a new line to the new point
        self.path.lineTo(point)

        # Set the new path
        self.path_ref.setPath(self.path)


def is_point_in_margin(a, b):
    '''
    Checks if point A is close to point B,
    by a margin of the constant POINT_MARGIN
    '''

    diff_x = a.x() - b.x()
    diff_y = a.y() - b.y()

    if diff_x > POINT_MARGIN or diff_x < -POINT_MARGIN:
        return False

    if diff_y > POINT_MARGIN or diff_y < -POINT_MARGIN:
        return False

    return True


def send_popup():
    popup_ok("Information", "Polygon closed",
             '''
            This polygon was closed.
            Unfortunately as of now you cannot edit it.

            Please delete it and create a new
            polygon if you want to change it.
            ''')
